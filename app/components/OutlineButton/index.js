import React from 'react';
import {
    View,
    Text,
    TouchableWithoutFeedback,
    StyleSheet
} from 'react-native';
//LOCAL
import config from '../../../config';

class OutlineButton extends React.PureComponent {
    render() {
        const {
            containerStyle,
            textStyle
        } = styles;

        return (
            <TouchableWithoutFeedback onPress={this.props.onPress}>
                <View style={[containerStyle, this.props.style]}>
                    <Text style={textStyle}>{ this.props.text }</Text>
                </View>
            </TouchableWithoutFeedback>
        );
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor:config.COLOR_PRIMARY,
        // borderWidth: 2,
        // borderColor: config.COLOR_PRIMARY,
        borderRadius: 8,
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 8
    },
    textStyle: {
        color: '#fff',
        fontSize: 20,
        fontFamily: 'OpenSans-Bold'
    }
});

export default OutlineButton;