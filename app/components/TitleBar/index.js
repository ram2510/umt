import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
//LOCAL
import config from '../../../config';

class TitleBar extends React.PureComponent {
    render() {
        const {
            containerStyle,
            titleTextStyle
        } = styles;

        return (
            <View style={containerStyle}>
                <Icon onPress={ this.props.leftIconFunction } name={this.props.leftIconName} color={config.COLOR_TEXT_DARK} size={32} />
                <Image style={titleTextStyle} source={ require('../../../assets/images/logo.png') } />
                <Icon onPress={ this.props.rightIconFunction } name={this.props.rightIconName} color={config.COLOR_TEXT_DARK} size={32} />
            </View>
        );
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        padding: 16
    },
    titleTextStyle: {
        width: 190,
        height: 49,
        alignSelf: 'center'
    }
});

export default TitleBar;