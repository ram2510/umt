import React, { Component } from 'react';
import { View,Button,Alert } from 'react-native';
// import firebase from 'firebase/app';
import { LoginManager,AccessToken,GraphRequest,GraphRequestManager} from 'react-native-fbsdk';
// import axios from 'axios'
// import { withNavigation } from 'react-navigation'
// import config from '../../../config';


export default class FBLoginButton extends Component {
    
    _responseInfoCallback = (error, result) => {
        if (error) {
          alert('Error fetching data: ' + error.toString());
        } else {
            this.props.login(result)
        }
      }
      
      fbLogin=()=>{
          
        LoginManager.logInWithPermissions(["email"])
            .then(result => {
                if (result.isCancelled) {
                console.log("Login was cancelled");
                }
                AccessToken.getCurrentAccessToken().then(data=>{
                    const infoRequest = new GraphRequest(
                        '/me?fields=name,email',
                        null,
                        this._responseInfoCallback
                      );
                      // Start the graph request.
                      new GraphRequestManager().addRequest(infoRequest).start();
                })
            })
            // .then(data => {
            //     this.props.login(data)
            //     // console.log(data);
            //     // const credential = firebase.auth.FacebookAuthProvider.credential(
            //     // data.accessToken
            //     // );
            //     // let user = firebase.auth().currentUser.providerData[0]
            //     // let userId = user.uid
            //     // axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
            //     //     .then(resp => {
            //     //         const token = resp.data.token;
            //     //         axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/' + userId, { headers: ({ 'Authorization': 'Bearer ' + token }) })
            //     //             .then(resp => { console.log(resp)})
            //     //             .catch(err => Alert.alert("lol", err));
            //     //     })
            //     //     .catch(err => this.showDialogWithMessage(JSON.stringify(err)))
            //     //     .finally(() => this.setState({ loading: false }));
            //     })
                .catch(error => {
                    console.log("Failed", error);
                });
                    
      }

      showDialogWithMessage(message) {
        Alert.alert(config.APP_NAME, message);
    }
    
    render() {
        return (
            <View style={{
                
                alignSelf: 'center',
                marginTop: 8
            }}
            >
            <Button 
            onPress={this.fbLogin}
            title="Continue with fb"
            color="#4267B2"
            />
            
            </View>
        );
    }
};
//  withNavigation(FBLoginButton)
