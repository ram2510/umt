import React from 'react';
import {
    ScrollView,
    View,
    Text,
    Image,
    FlatList,
    StyleSheet,
    Button,
    Alert,
    TouchableWithoutFeedback
} from 'react-native';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import SpinnerScreen from '../../components/SpinnerScreen'
import AsyncStorage from '@react-native-community/async-storage';

class Settings extends React.Component {
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    state={
        loading:true,
    }

    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })

    }

    logout=async()=>{
        const keys = ['userData','token']
        try {
            await AsyncStorage.multiRemove(keys)
            this.props.navigation.navigate('SignIn')
        } catch (error) {
            // console.log(error)
            this.showMessageDialog("An Unexpected error ocurred while tryng to logout please try again later")
        }
    }
    showMessageDialog(message) {
        Alert.alert(config.APP_NAME, message);
    }
    

    render() {

        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle
        } = styles;
        const { navigation } = this.props;
        const user = this.user
        const token = this.token
        return (
            <ScrollView style={containerStyle}>
            <View style={contentStyle}>
                <TitleBar
                        leftIconName='ios-arrow-round-back'
                        leftIconFunction={ () => navigation.goBack() }
                        titleText={ config.APP_NAME }
                    />

                <View style={coachItemStyle}>
                    <Image
                        style={coachImageStyle}
                        source={{uri: user.image||'https://fakeimg.pl/128x128/'}}
                    />
                    <View style={coachContentStyle}>
                        <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{user.name}</Text>
                        <Text style={{borderWidth: 0, fontSize: 16}}>{user.email}</Text>
                        <Text style={{borderWidth: 0, fontSize: 16}}>{user.bio}</Text>
                        <Button
                            title='Edit Profile'
                            color={config.COLOR_PRIMARY}
                            onPress={ () => navigation.navigate('EditProfile', { user: user }) }
                        />
                    </View>
                </View>

                <OutlineButton
                    text='Bookings'
                    style={{marginTop: 32}}
                    onPress={ () => navigation.navigate('UserBookings', { user: user , token: token}) }

                />

                <OutlineButton
                    text='About'
                    style={{marginTop: 20}}
                />

                <OutlineButton
                    text='Sign Out'
                    style={{marginTop: 20, marginBottom: 20}}
                    onPress={this.logout}
                />
            </View>
            </ScrollView>
        );
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 128,
        marginBottom: 32
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-evenly',
        marginLeft: 24
    }
});

export default Settings;