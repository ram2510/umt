import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    Button,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import SimpleButton from '../../components/SimpleButton';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class UpcomingBookings extends React.Component {
    state = {
        bookings: [],
        // counsellor: ''
        loading:true,
        empty:false

    }

    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        // this.setState({
        //     loading:false
        // })
    }

    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle
        } = styles;
        const { navigation } = this.props;
        
        // const coachingType = navigation.getParam('type', null);
        // const coachingTypeName = coachingType.key;
        const user = this.user
        const token = this.token
        return (
            <ScrollView style={containerStyle}>
                <View style={contentStyle}>
                    <TitleBar
                        leftIconName='ios-arrow-round-back'
                        leftIconFunction={() => navigation.goBack()}
                        titleText='Bookings'
                    />
                    <View style={{ flexDirection: 'row' }}>
                        {/*To set the FirstScreen*/}
                        <SimpleButton
                            text='Past'
                            style={{ marginTop: 8 , width: '30%'}}
                            onPress={ () => navigation.navigate('UserBookings', { user: user , token: token}) }
                            />
                        <SimpleButton
                            text='Upcoming'
                            style={{ marginTop: 8  , width: '30%'}}
                            onPress={ () => navigation.navigate('UpcomingBookings', { user: user , token: token}) }
                            />


                    </View>
                 


                    {(this.state.empty?
                    <View style={styles.empty}>
                        <Text style={styles.emptyText}>No bookings to show</Text>
                    </View>:
                    <FlatList
                        data={this.state.bookings}
                        renderItem={({ item }) => this.renderPastListItem(item, user, token)}
                        keyExtractor={(item, index) => index}
                    />)}
                </View>
            </ScrollView>
        );
    }

    renderPastListItem(bookings, user, token) {
        const {
            bookingImageStyle,
            bookingItemStyle,
            bookingContentStyle,
        } = styles;
        // const { navigation } = this.props;

        // const token = navigation.getParam('token', null);
        // const user = navigation.getParam('user', null);

        // axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/booking/patients/' + user, { headers: ({ 'Authorization': 'Bearer ' + token }) })
        //     .then(resp => {
        //         const bookings = resp.data;
        //         this.setState({ bookings: bookings });


        //         // axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/counsellors/' + bookings.counsellor_id, {headers: ({'Authorization': 'Bearer ' + token }) })
        //         //     .then(response => {
        //         //         const counsellor = response.data;
        //         //         this.setState({ counsellor: counsellor });
        //         //     })
        //         //     .catch(err => Alert.alert(config.APP_NAME, JSON.stringify(err)));



        //     })
        //     .catch(err => Alert.alert(config.APP_NAME, JSON.stringify(err)));

        return (
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user, token: token })}>
                <View style={bookingItemStyle}>
                     <Image
                        style={bookingImageStyle}
                        source={{ uri: coach.image || 'https://fakeimg.pl/128x128/' }}
                    />
                    <View style={bookingContentStyle}>
                        <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold' }}>{bookings.counsellor_id}</Text>
                        <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular' }}>{coach.name}</Text> */}
                        <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular' }}>{bookings.counsellor_phone}</Text>
                        <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular' }}>{bookings.booking_timestamp}</Text>
{/* 
                         <Image source={ {uri: URI }
                        } style={starImageStyle} /> */}

                         {/* <Button
                            title='Book Now'
                            color={config.COLOR_PRIMARY}
                            onPress={() => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user })}
                        />  */}
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }



    async componentDidMount() {
        const { navigation } = this.props;
        let token = await AsyncStorage.getItem("token")
        token = JSON.parse(token)
        let user = await AsyncStorage.getItem('userData')
        user = JSON.parse(user)




        axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/booking/patients/' + user, { headers: ({ 'Authorization': 'Bearer ' + token }) })
            .then(resp => {
                const bookings = resp.data;
                this.setState({ bookings: bookings });

                if(bookings.length){
                axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/counsellors/' + bookings.counsellor_id, {headers: ({'Authorization': 'Bearer ' + token }) })
                    .then(response => {
                        const coach = response.data;
                        this.setState({ coach: coach,loading:false  });
                    })
                    .catch(err => {
                        Alert.alert(config.APP_NAME, config.ERROR_MESSAGE)
                        // console.log(err.response)
                        this.setState({
                            loading:false
                        })
                    });                
                }else{
                    this.setState({
                        loading:false,
                        empty:true
                    })
                }


            })
            .catch(err => {
                // console.log(err.status)
                if(err.status!==400){
                    Alert.alert(config.APP_NAME, config.ERROR_MESSAGE)
                    // console.log(err.response)
                }else{
                    // console.log("l")
                    this.setState({
                        loading:false,
                        empty:true
                    })
                }
            });
        }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    bookingItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 128,
    },
    bookingImageStyle: {
        height: 96,
        width: 96,
        borderRadius: 48
    },
    bookingContentStyle: {
        justifyContent: 'space-evenly',
        paddingLeft: 16
    },
    empty:{
        flex: 1,
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        marginTop: "50%",
        padding: 10
    },
    emptyText:{
        fontSize: 16,
        color: "#777"
    }

});

export default UpcomingBookings;
