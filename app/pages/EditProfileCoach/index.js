import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TextInput,
    Image,
    Alert,
    TouchableWithoutFeedback,
    Switch
} from 'react-native';
import axios from 'axios';
import firebase from 'firebase/app';
import 'firebase/storage';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
//LOCAL
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import config from '../../../config';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class EditProfile extends React.Component {
    state = {
        _id: null,
        name: '',
        image: null,
        email: '',
        points: 0,
        bio: '',
        description: '',
        rate: 0,
        links: [],
        last_active_latitude: 0,
        last_active_longitude: 0,
        phone: '',
        birthday: '',
        tags: [],
        timings: [],
        life: false,
        executive: false,
        relationship: false,
        career: false,
        performance: false,
        loading:true,
    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })

    }
    render() {

        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle,
            textInputStyle,
            categoriesStyle,
            categoryItemTextStyle,
            categoryItemViewStyle
        } = styles;
        const { navigation } = this.props;
        const coach = this.user

        return (
            <ScrollView style={containerStyle}>
            <View style={contentStyle}>
                <TitleBar
                    leftIconName='ios-arrow-round-back'
                    leftIconFunction={ () => navigation.goBack() }
                    titleText={ config.APP_NAME }
                />

                <View style={coachItemStyle}>
                    <TouchableWithoutFeedback onPress={ this.handleChoosePhoto.bind(this) }>
                        <Image
                            style={coachImageStyle}
                            source={{uri: this.state.image||'https://fakeimg.pl/128x128/'}}
                        />
                    </TouchableWithoutFeedback>
                    <View style={coachContentStyle}>
                        <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{this.state.name}</Text>
                        <Text style={{borderWidth: 0, fontSize: 16}}>{this.state.email}</Text>
                        <Text numberOfLines={1} style={{borderWidth: 0, fontSize: 16}}>{this.state.bio}</Text>
                    </View>
                </View>

                <TextInput
                    placeholder='Name'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.name}
                    onChangeText={ text => this.setState({ name: text }) }
                />

                <TextInput
                    placeholder='Bio'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.bio}
                    onChangeText={ text => this.setState({ bio: text }) }
                />

                <TextInput
                    placeholder='Description'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.description}
                    onChangeText={ text => this.setState({ description: text }) }
                />

                <TextInput
                    placeholder='Phone'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.phone}
                    onChangeText={ text => this.setState({ phone: text }) }
                />

                <TextInput
                    placeholder='Tags'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.tags}
                    onChangeText={ text => this.setState({ tags: text }) }
                />

                <TextInput
                    placeholder='Birthday (YYYY-MM-DD)'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.birthday}
                    onChangeText={ text => this.setState({ birthday: text }) }
                />

                <View style={categoriesStyle}>
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Life/Personal coaching' }</Text>
                        <Switch
                            value={ this.state.life }
                            onValueChange={ newVal => this.setState({ life: newVal }) }
                        />
                    </View>
                    
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Relationship coaching' }</Text>
                        <Switch
                            value={ this.state.relationship }
                            onValueChange={ newVal => this.setState({ relationship: newVal }) }
                        />
                    </View>
                    
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Executive coaching' }</Text>
                        <Switch
                            value={ this.state.executive }
                            onValueChange={ newVal => this.setState({ executive: newVal }) }
                        />
                    </View>

                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Career coaching' }</Text>
                        <Switch
                            value={ this.state.career } 
                            onValueChange={ newVal => this.setState({ career: newVal }) }
                        />                        
                    </View>

                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Performance coaching' }</Text>
                        <Switch
                            value={ this.state.performance }
                            onValueChange={ newVal => this.setState({ performance: newVal }) }
                        />
                    </View>
                </View>

                <OutlineButton
                    text='Update'
                    style={{marginTop: 32, marginBottom: 32}}
                    onPress={ this.onUpdateButtonPress.bind(this) }
                />
            </View>
            </ScrollView>
        );
    }

    onUpdateButtonPress() {
        const { navigation } = this.props;
        const coach = this.user
        const token = this.token
        const coachId = coach._id;
        const name = this.state.name;
        const bio = this.state.bio;
        const description = this.state.description;
        const phone = this.state.phone;
        const updateBody = {
            name: name,
            bio: bio,
            description: description,
            phone: phone,
            birthday: new Date(this.state.birthday).getTime()/1000
        };
        axios.put(config.HOSTNAME+'/counsellors/'+coachId, updateBody, { headers: ({ 'Authorization': 'Bearer ' + token }) }   )
            .then(resp => {
                navigation.navigate('HomeCoach', { coach: resp.data });
            })
            .catch(err => Alert.alert(config.APP_NAME, JSON.stringify(err)));
    }

    async componentDidMount() {
        const { navigation } = this.props;
        let coach = await AsyncStorage.getItem('userData')
        coach = JSON.parse(coach)
        this.setState({
            _id: coach._id,
            image: coach.image,
            name: coach.name,
            bio: coach.bio,
            description: coach.description,
            phone: coach.phone,
            birthday: coach.birthday,
            following: coach.following,
            email: coach.email,
            organization: coach.organization,
            role: coach.role,
            rate: coach.rate,
            life: coach.primary_categories.indexOf('life')>=0,
            relationship: coach.primary_categories.indexOf('relationship')>=0,
            executive: coach.primary_categories.indexOf('executive')>=0,
            performance: coach.primary_categories.indexOf('performance')>=0,
            career: coach.primary_categories.indexOf('career')>=0
        });
    }

    
    handleChoosePhoto() {
        const { navigation } = this.props;
        const coach = this.user
        const Blob = RNFetchBlob.polyfill.Blob
        const fs = RNFetchBlob.fs;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
        window.Blob = Blob;

        const options = {
            title: 'Select Profile Picture',
            path: 'images'
        };

        ImagePicker.showImagePicker(options, (resp) => {
            console.log(resp);
            if (resp.uri) {
                const imageUri = resp.uri;
                const imageRef = firebase.storage().ref('images').child(coach._id).child('dp.jpg');
                let mime = 'image/jpg';
                fs.readFile(imageUri, 'base64')
                    .then((data) => Blob.build(data, { type: `${mime};BASE64` }))
                    .then((blob) => imageRef.put(blob, { contentType: mime }))
                    .then(() => imageRef.getDownloadURL())
                    .then((imageUrl) => this.updateUserImage(imageUrl))
                    .catch(err => Alert.alert(config.APP_NAME, JSON.stringify(err)));
            }
        });
    }

    async updateUserImage(imageUrl) {
        const { navigation } = this.props;
        const coach = this.user
        const token = this.token

        axios.put(config.HOSTNAME+'/counsellors/'+coach._id, {  image: imageUrl }, { headers: ({ 'Authorization': 'Bearer ' + token }) } )
            .then(async resp => {
                const newUser = resp.data;
                await AsyncStorage.mergeItem('userData', JSON.stringify(newUser))
                navigation.navigate('HomeCoach', { coach: newUser });
                Alert.alert(config.APP_NAME, 'Profile updated');
            })
            .catch(err => Alert.alert(config.APP_NAME, JSON.stringify(err)));
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    textInputStyle: {

    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 128,
        marginBottom: 32
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-evenly',
        marginLeft: 24
    },
    textInputStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        marginTop: 16
    },
    categoriesStyle: {
        justifyContent: 'space-evenly'
    },
    categoryItemViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8
    },
    categoryItemTextStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        flex: 1
    }
});

export default EditProfile;