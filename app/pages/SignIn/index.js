import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Dimensions,
    TextInput,
    Image,
    ActivityIndicator,
    Alert,
    ImageBackground
} from 'react-native';
import {           
    GoogleSignin,
    GoogleSigninButton,
    statusCodes
} from 'react-native-google-signin';
import firebase from 'firebase/app';
import 'firebase/auth';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import OutlineButton from '../../components/OutlineButton';

import FBLoginButton from '../../components/FBLoginButton/FBLoginButton';
import SpinnerScreen from '../../components/SpinnerScreen'
import AsyncStorage from '@react-native-community/async-storage';
// import { async } from 'q';

//936662275132-0jn4fcgu31kocp676shkciulga7eukqg.apps.googleusercontent.com

// This class the default sign in page for the patient

class SignIn extends React.Component {
    state = {
        email: '',
        password: '',
        loading: false,
        userInfo:{}
    }

    constructor(props) {
        super(props);
        

        if (firebase.apps.length === 0) {
            const firebaseConfig = {
                apiKey: "AIzaSyAUXZctqy-8R1_CHEfVv6Qz5p7bz8dtDZU",
                authDomain: "auth-13850.firebaseapp.com",
                databaseURL: "https://auth-13850.firebaseio.com",
                projectId: "auth-13850",
                storageBucket: "auth-13850.appspot.com",
                messagingSenderId: "936662275132",
                appId: "1:936662275132:web:7c10825d3ab54621"
            };
            firebase.initializeApp(firebaseConfig);
            
        }
    }
    componentDidMount(){
        GoogleSignin.configure({ webClientId: config.WEB_CLIENT_ID, offlineAccess: false });
    }

    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loggin In .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            titleTextStyle,
            subtitleTextStyle,
            googleSignInButtonStyle,
            textInputStyle,
            imageStyle,
            signUpTextStyle,
            counsellorLoginTextStyle,
            backgroundImage,
            facebookSignInButtonStyle
        } = styles;

       

        return (
            <ImageBackground source={require('../../../assets/images/background.png')} style={backgroundImage} >

                <ScrollView style={containerStyle}>
                    <View style={contentStyle}>

                        <Image style={imageStyle} source={require('../../../assets/images/logo.png')} />

                        <GoogleSigninButton
                            style={googleSignInButtonStyle}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Light}
                            onPress={this.googleSignIn}
                        />

                        <FBLoginButton
                            style={facebookSignInButtonStyle}
                            login={this.loginFB}
                        />

                        <Text style={[subtitleTextStyle, { marginTop: 16, alignSelf: 'center' }]}>{'or'}</Text>

                        <TextInput
                            placeholder='Email Address'
                            selectionColor={config.COLOR_PRIMARY}
                            underlineColorAndroid={config.COLOR_PRIMARY}
                            placeholderTextColor={config.COLOR_TEXT_LIGHT}
                            style={textInputStyle}
                            keyboardType='email-address'
                            value={this.state.email}
                            onChangeText={text => this.setState({ email: text })}
                        />

                        <TextInput
                            placeholder='Password'
                            selectionColor={config.COLOR_PRIMARY}
                            underlineColorAndroid={config.COLOR_PRIMARY}
                            placeholderTextColor={config.COLOR_TEXT_LIGHT}
                            style={textInputStyle}
                            secureTextEntry
                            value={this.state.password}
                            onChangeText={text => this.setState({ password: text })}
                        />

                        {
                            this.state.loading ?
                                <ActivityIndicator
                                    size="large"
                                    color={config.COLOR_PRIMARY}
                                    style={{ marginTop: 32 }}
                                /> :
                                <OutlineButton
                                    text='Sign In'
                                    style={{ marginTop: 32 }}
                                    onPress={this.onSignInWithEmailButtonPress.bind(this)}
                                />
                        }

                        <Text style={signUpTextStyle} onPress={() => this.props.navigation.navigate('SignUpPatient')}>{'New here? Create an account.'}</Text>

                        <Text style={counsellorLoginTextStyle} onPress={() => this.props.navigation.navigate('SignInCoach')}>{'If you are a coach, click here to login.'}</Text>
                    </View>
                </ScrollView>
            </ImageBackground>
        );
    }
    storeData=async (obj)=>{
        try {
            await AsyncStorage.setItem('userData', JSON.stringify(obj))
          } catch (e) {
            // saving error
            console.log(e)
            this.showDialogWithMessage("Something went wrong")
            return	
          }
    }
		storeToken=async (obj) =>{
			try {
				await AsyncStorage.setItem('token', JSON.stringify(obj))
			} catch (e) {
				// saving error
				console.log(e)
				this.showDialogWithMessage("Something went wrong")
				return	
			}
		}


    showDialogWithMessage=(message) =>{
        Alert.alert(config.APP_NAME, message);
    }

    loginFB=async (data)=>{
        try{
            this.setState({
                loading:true
            })
        let userId = data.id
            let tokenResp = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
            const token = tokenResp.data.token;
            let userData = await axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/' + userId,{
                headers:{
                    Authorization: 'Bearer ' + token                
                }
            })

            // this.props.navigation.navigate('HomePatient', { patient: userData.data, token: token })
            // await GoogleSignin.revokeAccess()
                // console.log(userData)
            this.setState({
                loading:false
            },async ()=>{
							userData.data["userType"]="Patient"
							await this.storeData(userData.data)
							await this.storeToken(token)
							// console.log(userData)
              this.props.navigation.navigate('HomePatient', { patient: userData.data, token: token })
            })
          } catch (error) {
            //   console.log(error)
            if(error.response.status===400){
                // console.log("l")
                try {
                    let tokenResp = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + data.id)
                    const token = tokenResp.data.token;
                    let jsonBody ={
                        "_id":data.id,
                        "name":data.name,
                        "email":data.email
                    }
                    let headers = {
                        headers:{
                            Authorization: 'Bearer ' + token
                        }
                    }    
                    let userData = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/', jsonBody,headers)
                    // console.log(userData)   
                    this.setState({
                        loading:false
                    },async()=>{
                        this.props.navigation.navigate('HomePatient', { patient: userData.data, token: token })
                    })
                    return
                }catch(err){
                    this.setState({
                        loading:false
                    })
                    console.log(err)
                    Alert.alert('Something went wrong')
            }
        }
        this.setState({
            loading:false
        })
        Alert.alert('Something went wrong')
    }

    }

    async onSignInWithEmailButtonPress() {
        this.setState({ loading: true });
        firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
            .then(response => {
                const userId = response.user.uid;
                axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
                    .then(resp => {
                        const token = resp.data.token;
												axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/' + userId, { headers: ({ 'Authorization': 'Bearer ' + token }) })
												
                            .then(async (resp) => { 
                                resp.data["userType"]="Patient"
                                await this.storeData(resp.data)
                                await this.storeToken(token)
                                this.props.navigation.navigate('HomePatient', { patient: resp.data, token: token }) 
                            })
                            .catch(err => Alert.alert(APPNAME, err));
                    })
                    .catch(err => this.showDialogWithMessage(JSON.stringify(err)))
                    .finally(() => this.setState({ loading: false }));
            });
    }

    googleSignIn=async ()=>{
        try {
            this.setState({
                loading:true
            })
            // console.log("l")
            await GoogleSignin.hasPlayServices();
            // console.log("l")
            const response = await GoogleSignin.signIn();
            this.setState({ userInfo:response.user, error: null });
            // const credential = await firebase.auth.GoogleAuthProvider.credential(response.idToken, response.accessToken);
            // let user = await firebase.auth().signInWithCredential(credential)
            // console.log(response)
            let userId = response.user.id
            let tokenResp = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
            const token = tokenResp.data.token;
            let userData = await axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/' + userId,{
                headers:{
                    Authorization: 'Bearer ' + token                
                }
            })
            // console.log(userData)
            this.setState({
                loading:false
            },async ()=>{
								userData.data["userType"]="Patient"
								await this.storeData(userData.data)
								await this.storeToken(token)
                this.props.navigation.navigate('HomePatient', { patient: userData.data, token: token })
            })
            // await GoogleSignin.revokeAccess()
            // this.props.navigation.navigate('HomePatient', { patient: user, token: token })
          } catch (error) {
            //   console.log(error)
            if(error.response.status===400){
                // console.log("l")
                try {
                    let tokenResp = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + this.state.userInfo.id)
                    const token = tokenResp.data.token;
                    let jsonBody ={
                        "_id":this.state.userInfo.id,
                        "name":this.state.userInfo.name,
                        "email":this.state.userInfo.email
                    }
                    let headers = {
                        headers:{
                            Authorization: 'Bearer ' + token
                        }
                    }    
                    let userData = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/patients/', jsonBody,headers)
                    // console.log(userData)   
                    this.setState({
                        loading:false
                    },async ()=>{
                        userData.data["userType"]="Patient"
                        await this.storeData(userData.data)
                        await this.storeToken(token)
                        this.props.navigation.navigate('HomePatient', { patient: userData.data, token: token })
                    })
 
                } catch (error) {
                    this.setState({
                        loading:false
                    })
                    console.log(error.response)
                    Alert.alert("Something went wrong")
                }
            }
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
              // sign in was cancelled
            //   Alert.alert('cancelled');
            } else if (error.code === statusCodes.IN_PROGRESS) {
              // operation in progress already
              Alert.alert('in progress');
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
              Alert.alert('play services not available or outdated');
            } else {
            //   Alert.alert('Something went wrong', error.toString());
            //   console.log(error.toString().response)
            }
            this.setState({
                loading:false
            })
            Alert.alert("Something went wrong")
          }
    }

    createNewUserOnSystem=(uid, name, email, token)=> {
        const postBody = {
            _id: uid,
            name: name,
            email: email
        };
        axios.post(config.HOSTNAME + '/patients', { headers: ({ 'Authorization': 'Bearer ' + token }) }, postBody)
            .then(resp => this.props.navigation.navigate('HomePatient', { patient: user, token }))
            .catch(err => this.showDialogWithMessage(JSON.stringify(err)));
    }


};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        // backgroundColor: 'white'
    },
    contentStyle: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 64
    },
    imageStyle: {
        height: 49,
        width: 190,
        marginBottom: 32,
        marginTop: 32,
        alignSelf: 'center'
    },
    titleTextStyle: {
        color: config.COLOR_PRIMARY,
        fontSize: 32,
        fontFamily: 'OpenSans-Bold'
    },
    subtitleTextStyle: {
        color: config.COLOR_TEXT_DARK,
        fontFamily: 'OpenSans-Regular',
        fontSize: 32
    },
    googleSignInButtonStyle: {
        width: Dimensions.get('window').width * 0.75,
        height: 48,
        alignSelf: 'center',
        marginTop: 24
    },
    facebookSignInButtonStyle: {
        width: Dimensions.get('window').width * 0.75,
        height: 48,
        alignSelf: 'center',
        marginTop: 8
    },
    textInputStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        marginBottom: 16
    },
    signUpTextStyle: {
        marginTop: 8,
        fontSize: 16,
        color: config.COLOR_TEXT_DARK,
        fontFamily: 'OpenSans-Regular',
        alignSelf: 'center'
    },
    counsellorLoginTextStyle: {
        marginTop: 32,
        fontSize: 16,
        color: config.COLOR_TEXT_LIGHT,
        fontFamily: 'OpenSans-Regular',
        alignSelf: 'center'
    },
    backgroundImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
        width: null,
        height: null,
    }
});

export default SignIn;
