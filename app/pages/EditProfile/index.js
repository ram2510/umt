import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TextInput,
    Image,
    Alert,
    TouchableWithoutFeedback
} from 'react-native';
import axios from 'axios';
import firebase from 'firebase/app';
import 'firebase/storage';
import ImagePicker from 'react-native-image-picker';
import RNFetchBlob from 'rn-fetch-blob';
//LOCAL
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import config from '../../../config';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class EditProfile extends React.Component {
    state = {
        _id: null,
        name: '',
        image: null,
        organization: '',
        role: '',
        points: 0,
        following: [],
        bio: '',
        email: '',
        phone: '',
        birthday: '',
        loading:false
    }
    constructor(props){
        super(props)
        if (firebase.apps.length === 0) {
            const firebaseConfig = {
                apiKey: "AIzaSyAUXZctqy-8R1_CHEfVv6Qz5p7bz8dtDZU",
                authDomain: "auth-13850.firebaseapp.com",
                databaseURL: "https://auth-13850.firebaseio.com",
                projectId: "auth-13850",
                storageBucket: "auth-13850.appspot.com",
                messagingSenderId: "936662275132",
                appId: "1:936662275132:web:7c10825d3ab54621"
            };
            firebase.initializeApp(firebaseConfig);
            
        }
    }
    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Updating .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle,
            textInputStyle
        } = styles;
        const { navigation } = this.props;

        return (
            <ScrollView style={containerStyle}>
            <View style={contentStyle}>
                <TitleBar
                    leftIconName='ios-arrow-round-back'
                    leftIconFunction={ () => navigation.goBack() }
                    titleText={ config.APP_NAME }
                />

                <View style={coachItemStyle}>
                    <TouchableWithoutFeedback onPress={ this.handleChoosePhoto.bind(this) }>
                        <Image
                            style={coachImageStyle}
                            source={{uri: this.state.image||'https://fakeimg.pl/128x128/'}}
                        />
                    </TouchableWithoutFeedback>
                    <View style={coachContentStyle}>
                        <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{this.state.name}</Text>
                        <Text style={{borderWidth: 0, fontSize: 16}}>{this.state.email}</Text>
                        <Text style={{borderWidth: 0, fontSize: 16}}>{this.state.bio}</Text>
                    </View>
                </View>

                <TextInput
                    placeholder='Name'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.name}
                    onChangeText={ text => this.setState({ name: text }) }
                />

                <TextInput
                    placeholder='Bio'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.bio}
                    onChangeText={ text => this.setState({ bio: text }) }
                />

                <TextInput
                    placeholder='Phone'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.phone}
                    onChangeText={ text => this.setState({ phone: text }) }
                    keyboardType="phone-pad"
                />

                <TextInput
                    placeholder='Birthday (YYYY-MM-DD)'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.birthday}
                    onChangeText={ text => this.setState({ birthday: text }) }
                />

                <OutlineButton
                    text='Update'
                    style={{marginTop: 32}}
                    onPress={ this.onUpdateButtonPress.bind(this) }
                />
            </View>
            </ScrollView>
        );
    }

   async  onUpdateButtonPress() {
       this.setState({
           loading:true
       })
        const _id = this.state._id;
        const name = this.state.name;
        const bio = this.state.bio;
        const phone = this.state.phone;
        let birthday = new Date(this.state.birthday).getTime();
        birthday = Math.round(birthday/1000)
        if(birthday){
            const updateObj = { _id, name, bio, phone, birthday };
            let token = await AsyncStorage.getItem('token')
            token = JSON.parse(token)
            axios.put(config.HOSTNAME+'/patients/'+_id, updateObj,{headers:{ 'Authorization': 'Bearer ' + token }})
                .then(async resp => {
                    await AsyncStorage.mergeItem('userData',JSON.stringify(resp.data))
                    let user = resp.data
                    let date = new Date(birthday*1000)
                    let year = date.getFullYear()
                    let month = date.getMonth() +1
                    let day = date.getDate()
                    birthday = year + "-" + month + "-" + day                    
                    if(birthday){
                        this.setState({
                            _id: user._id,
                            name: user.name,
                            bio: user.bio,
                            phone: user.phone,
                            birthday: birthday,
                            image: user.image,
                            following: user.following,
                            email: user.email,
                            organization: user.organization,
                            role: user.role,
                            loading:false
                        })
                    }else{
                        this.setState({
                            loading:false
                        })
                        Alert.alert(config.APP_NAME,"Error while trying to update profile please try again later")
                    }
                })
                .catch(err =>{ 
                    this.setState({
                        loading:false
                    })
                    Alert.alert(config.APP_NAME, JSON.stringify(err))
                    console.log(err.response)
                });
        }else{
            this.setState({
                loading:false
            })
            Alert.alert(config.APP_NAME,"Error while trying to update profile please try again later")
        }
    }

    async componentDidMount() {
        const { navigation } = this.props;
        let user = await AsyncStorage.getItem('userData')
        user = JSON.parse(user);
        birthday = user.birthday
        // console.log(birthday)
        let date = new Date(birthday*1000)
        let year = date.getFullYear()
        let month = date.getMonth() +1
        let day = date.getDate()
        birthday = year + "-" + month + "-" + day
        this.setState({
            _id: user._id,
            name: user.name,
            bio: user.bio,
            phone: user.phone,
            birthday: birthday,
            image: user.image,
            following: user.following,
            email: user.email,
            organization: user.organization,
            role: user.role
        });
    }

    
    async handleChoosePhoto() {
        // const { navigation } = this.props;
        let user = await AsyncStorage.getItem('userData')
        user = JSON.parse(user)
        // console.log(user)
        const Blob = RNFetchBlob.polyfill.Blob
        const fs = RNFetchBlob.fs;
        window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
        window.Blob = Blob;
        const options = {
            title: 'Select Profile Picture',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        ImagePicker.showImagePicker(options, (resp) => {
            // console.log(resp);"storage/object-not-found"
            if (resp.uri) {
                this.setState({
                    loading:true
                })
                const imageUri = resp.uri;
                const imageRef = firebase.storage().ref('images').child(user._id).child('dp.jpg');
                let mime = 'image/jpg';
                fs.readFile(imageUri, 'base64')
                    .then((data) => Blob.build(data, { type: `${mime};BASE64` }))
                    .then(async (blob) => {
                        try {
                            await imageRef.put(blob, { contentType: mime })
                            let downloadUrl = await  imageRef.getDownloadURL()
                            this.updateUserImage(downloadUrl)
                        } catch (error) {
                            Alert.alert(config.APP_NAME, config.ERROR_MESSAGE)
                            // console.log(err)
                        }
                    })
                   
            }
        });
    }

    async updateUserImage(imageUrl) {
        const { navigation } = this.props;
        let user = await AsyncStorage.getItem('userData')
        let token = await AsyncStorage.getItem('token')
        user = JSON.parse(user)
        token = JSON.parse(token)
        axios.put(config.HOSTNAME+'/patients/'+user._id, { image: imageUrl },{headers:{ 'Authorization': 'Bearer ' + token }})
            .then(async resp => {
                const newUser = resp.data;
                // navigation.navigate('HomePatient', { user: newUser });
                await AsyncStorage.mergeItem('userData',JSON.stringify(newUser))
                this.setState({
                    loading:false,
                    image:newUser.image
                },()=>{
                    Alert.alert(config.APP_NAME, 'Profile picture updated')
                })
            })
            .catch(err => {
                Alert.alert(config.APP_NAME, config.ERROR_MESSAGE)
                // console.log(err.response)
            });
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    textInputStyle: {

    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 128,
        marginBottom: 32
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-evenly',
        marginLeft: 24
    },
    textInputStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        marginTop: 16
    }
});

export default EditProfile;