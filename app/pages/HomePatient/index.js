import React from 'react';
import {
    ScrollView,
    View,
    StyleSheet,
    Text,
    Alert,
    FlatList,
    Image,
    Dimensions,
    TouchableWithoutFeedback,
    ImageBackground
} from 'react-native';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'
import life from '../../../assets/images/life.webp'
import career from '../../../assets/images/career.webp'
import executive from '../../../assets/images/executive.webp'
import performance from '../../../assets/images/performance.webp'
import relationship from '../../../assets/images/relationship.webp'

class HomePatient extends React.Component {
    state = {
        user: null,
        categoies: [
            { key: 'Life/Personal Coaching', value: 'life',imgURI:life },
            { key: 'Relationship Coaching', value: 'relationship',imgURI:relationship },
            { key: 'Executive Coaching', value: 'executive',imgURI:executive  },
            { key: 'Career Coaching', value: 'career',imgURI:career  },
            { key: 'Performance Coaching', value: 'performance',imgURI:performance  },
        ],
        loading:true
    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }

    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })
    }
    
    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            categoryItemStyle,
            categoryItemTextStyle,
            categoryImageStyle
        } = styles;

        const { navigation } = this.props;
        const user = this.user
        const token = this.token
        // console.log(user)
        // console.log(token)
        return (
            <ScrollView style={containerStyle}>
                <View>
                    <TitleBar
                        leftIconName='ios-search'
                        rightIconName='ios-settings'
                        titleText={config.APP_NAME}
                        rightIconFunction={() => navigation.navigate('Settings', { user: user, token: token })}
                        leftIconFunction={() => navigation.navigate('SearchPage', { user: user, token: token })}

                    />
                    <View style={contentStyle}>
                        <FlatList
                            data={this.state.categoies}
                            renderItem={({ item }) =>
                                <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachesListPage', { type: item, user: user, token: token })}>
                                    <View style={categoryItemStyle}>
                                        <ImageBackground
                                            style={categoryImageStyle}
                                            source={item.imgURI}
                                            blurRadius={1}
                                        >

                                            <Text style={categoryItemTextStyle}>{item.key}</Text>

                                        </ImageBackground>
                                    </View>
                                </TouchableWithoutFeedback>
                            }
                        />
                    </View>
                </View>
            </ScrollView>
        );
    }

    showMessageDialog(message) {
        Alert.alert(config.APP_NAME, message);
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    categoryItemStyle: {
        marginTop: 8,
        marginBottom: 16,
        borderRadius: 8,
        justifyContent: 'center',
        alignItems: 'center'
    },
    categoryItemTextStyle: {
        color: config.COLOR_TEXT_WHITE,
        fontSize: 20,
        fontFamily: 'OpenSans-Regular',
        margin: 2,
        textAlign: 'center',
        position: 'absolute',
        fontWeight: "bold"
    },
    categoryImageStyle: {
        height: 164,
        width: Dimensions.get('window').width,
        justifyContent: 'center',
        alignItems: 'center'
    }
});

export default HomePatient;