import React from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
    Text,
    Image,
    Button,
    FlatList,
    Alert,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class CoachProfilePage extends React.Component {
    state = {
        feedback: [],
        loading:true,
        loadingFeedback:true
    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })
    }
    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle,
            starImageStyle
        } = styles;

        const { navigation } = this.props;
        const coach = navigation.getParam('coach', null);
        const token = this.token
        const user = this.user
        var URI = "https://i.ibb.co/Rzpf2cB/zero.png";



        if (coach.rating == 5) {
            URI = "https://i.ibb.co/6PzSjWf/five.png"
        } else if (coach.rating == 4) {
            URI = "https://i.ibb.co/8bWGFRQ/four.png"
        } else if (coach.rating == 3) {
            URI = "https://i.ibb.co/pbL052Q/three.png"
        } else if (coach.rating == 2) {
            URI = "https://i.ibb.co/sP89GwK/two.png"
        } else if (coach.rating == 1) {
            URI = "https://i.ibb.co/PgC6hZw/one.png"
        } else if (coach.rating == 0) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        } else if (coach.rating == null) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        }


        return (
            <ScrollView style={containerStyle}>
                <View style={contentStyle}>
                    <TitleBar
                        leftIconName='ios-arrow-round-back'
                        leftIconFunction={() => navigation.goBack()}
                        titleText={config.APP_NAME}
                    />


                    <View style={[{
                        width: "100%", borderBottomColor: config.COLOR_PRIMARY, borderBottomWidth: 1, padding:10
                    }]}>


                        <View style={[{ flexDirection: 'row', width: "70%" }]}>
                            <Image
                                style={coachImageStyle}
                                source={{
                                    uri: coach.image || 'https://fakeimg.pl/128x128/'
                                }}
                            />
                            <View style={[{ width: "100%", paddingLeft: 16, paddingRight: 16 }]}>
                                <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold', marginBottom: 3 }}>{coach.name}</Text>
                                <Image source={{ uri: URI }
                                } style={starImageStyle} />
                                <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.bio}</Text>

                                <View style={[{ width: "70%", paddingBottom: 8, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row', marginTop: 20 }]}>

                                    <OutlineButton
                                        text='Book Now'
                                        // color={config.COLOR_PRIMARY}
                                        onPress={() => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user, token: token })}
                                    />
                                </View>
                            </View>
                        </View>


                    </View>

                    {/* <View style={coachItemStyle}> */}
                    {/* <Image
                        style={coachImageStyle}
                        source={{uri: coach.image||'https://fakeimg.pl/128x128/'}}
                    /> */}
                    {/* <View style={coachContentStyle}>
                        <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{coach.name}</Text>
                        <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular'}}>{coach.bio}</Text>

                        <Image source={ {uri: URI }
                        } style={starImageStyle} />
                        
                        {/* <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular', marginBottom: 8}}>{coach.rating}</Text> */}

                    {/* <Button
                            title='Book'
                            color={config.COLOR_PRIMARY}
                            onPress={ () => navigation.navigate('CoachBookPage', { coach: coach, token: token }) }
                        />
                    // </View> */} 
                {/* </View> */}

                    <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular', marginTop: 16 }}>{coach.description}</Text>

                    <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold', marginTop: 16,marginBottom: 12 }}>{'Feedback'}</Text>
                    {(this.state.loadingFeedback?
                    <View style={{width: "100%",justifyContent: "center",alignItems: "center"}}>
                        <ActivityIndicator color="#D3D3D3" size='large' />
                        <Text style={{fontSize: 20,color: "#777"}}>Loading Feedback ...</Text>
                    </View>
                        :<FlatList
                        data={this.state.feedback}
                        renderItem={({ item }) => this.renderFeedbackListItem(item)}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={this.listEmptyComponent}
                    />)}
                    
                </View>
            </ScrollView>
        );
    }

    listEmptyComponent=()=>{
        return(
            <View style={{width: "100%",justifyContent: "center",alignItems: "center"}}>
                <Text style={{fontSize: 20,color: "#777"}}>No feedbacks</Text>
            </View>
        )
    }

    renderFeedbackListItem(feedback) {
        const {
            feedBackItemImageStyle,
            feedbackItemStyle,
            coachContentStyle
        } = styles;
        
        return (
            <View style={feedbackItemStyle}>
                <Image
                    style={feedBackItemImageStyle}
                    source={{ uri: feedback.image || 'https://fakeimg.pl/128x128/' }}
                />
                <View style={[coachContentStyle, { paddingLeft: 16 }]}>
                    <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold' }}>{feedback.name}</Text>
                    <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular' }}>{feedback.content}</Text>
                    <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', marginBottom: 8 }}>{feedback.rating}</Text>
                </View>
            </View>
        );
    }

    async componentDidMount() {
        const { navigation } = this.props;
        const coach = navigation.getParam('coach', null);
        let token = await AsyncStorage.getItem("token")
        token = JSON.parse(token)
        // console.log(coach)
        axios.get(config.HOSTNAME + '/feedback/counsellors/' + coach._id,{headers:{ 'Authorization': 'Bearer ' + token }})
            .then(resp => {
                const feedbacks = [];
                resp.data.map(feedback => {
                    const username = feedback.source_username;
                    const userimage = feedback.source_user_image;
                    const content = feedback.content;
                    const rating = feedback.rating;
                    feedbacks.push({ name: username, image: userimage || 'https://fakeimg.pl/64x64/', content: content, rating: rating });
                });
                this.setState({ feedback: feedbacks,loadingFeedback:false });
            })
            .catch(err => {
                Alert.alert(config.APP_NAME, JSON.stringify(err))
                console.log(err.response)
                this.setState({
                    loadingFeedback:false
                })
            });
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        height: 128
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-evenly'
    },
    feedBackItemImageStyle: {
        height: 64,
        width: 64,
        borderRadius: 32
    },
    feedbackItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 80
    },
    starImageStyle: {
        height: 28,
        width: 100,
        resizeMode: 'cover',
    }
});

export default CoachProfilePage;