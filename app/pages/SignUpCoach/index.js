import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Dimensions,
    TextInput,
    Image,
    ActivityIndicator,
    Alert,
    Switch
} from 'react-native';
import firebase from 'firebase/app';
import 'firebase/auth';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import OutlineButton from '../../components/OutlineButton';

class SignUpCoach extends React.Component {
    state = {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        loading: false,
        bio: '',
        description: '',
        tags: '',
        life: false,
        relationship: false,
        executive: false,
        career: false,
        performance: false,
    }

    constructor(props) {
        super(props);
        if(firebase.apps.length===0) {
            const firebaseConfig = {
                apiKey: "AIzaSyCXCVM0c7ecSFM-jJ7rO_sQ_b6_ATE8Qno",
                authDomain: "unlock-me-today.firebaseapp.com",
                databaseURL: "https://unlock-me-today.firebaseio.com",
                projectId: "unlock-me-today",
                storageBucket: "",
                messagingSenderId: "1009927620710",
                appId: "1:1009927620710:web:68b689ea326995a4"
            };
            firebase.initializeApp(firebaseConfig);
        }
    }

    render() {
        const {
            containerStyle,
            contentStyle,
            titleTextStyle,
            subtitleTextStyle,
            textInputStyle,
            imageStyle,
            categoriesStyle,
            categoryItemViewStyle,
            categoryItemTextStyle
        } = styles;

        return (
            <ScrollView style={containerStyle}>
                <View style={contentStyle}>

                <Image style={imageStyle} source={ require('../../../assets/images/logo.png') } />

                <TextInput
                    placeholder='Name'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.name}
                    onChangeText={ text => this.setState({ name: text }) }
                />

                <TextInput
                    placeholder='Email Address'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    keyboardType='email-address'
                    value={this.state.email}
                    onChangeText={ text => this.setState({ email: text }) }
                />

                <TextInput
                    placeholder='Password'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    secureTextEntry
                    value={this.state.password}
                    onChangeText={ text => this.setState({ password: text }) }
                />

                <TextInput
                    placeholder='Confirm Password'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    secureTextEntry
                    value={this.state.confirmPassword}
                    onChangeText={ text => this.setState({ confirmPassword: text }) }
                />

                <TextInput
                    placeholder='Tags (separated by commas)'
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    placeholderTextColor={config.COLOR_TEXT_LIGHT}
                    style={textInputStyle}
                    value={this.state.tags}
                    onChangeText={ text => this.setState({ tags: text }) }
                />

                <View style={categoriesStyle}>
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Life/Personal coaching' }</Text>
                        <Switch
                            value={ this.state.life }
                            onValueChange={ newVal => this.setState({ life: newVal }) }
                        />
                    </View>
                    
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Relationship coaching' }</Text>
                        <Switch
                            value={ this.state.relationship }
                            onValueChange={ newVal => this.setState({ relationship: newVal }) }
                        />
                    </View>
                    
                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Executive coaching' }</Text>
                        <Switch
                            value={ this.state.executive }
                            onValueChange={ newVal => this.setState({ executive: newVal }) }
                        />
                    </View>

                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Career coaching' }</Text>
                        <Switch
                            value={ this.state.career } 
                            onValueChange={ newVal => this.setState({ career: newVal }) }
                        />                        
                    </View>

                    <View style={categoryItemViewStyle}>
                        <Text style={categoryItemTextStyle}>{ 'Performance coaching' }</Text>
                        <Switch
                            value={ this.state.performance }
                            onValueChange={ newVal => this.setState({ performance: newVal }) }
                        />
                    </View>
                </View>

                {
                    this.state.loading?
                    <ActivityIndicator
                        size="large"
                        color={config.COLOR_PRIMARY}
                        style={{marginTop: 32}}
                    /> :
                    <OutlineButton
                        text='Sign Up'
                        style={{marginTop: 32, marginBottom: 16}}
                        onPress={ this.onSignUpButtonPress.bind(this) }
                    />
                }

                </View>
            </ScrollView>
        );
    }

    showDialogWithMessage(message) {
        Alert.alert(config.APP_NAME, message);
        this.setState({ loading: false });
    }

    onSignUpButtonPress() {
        if(this.state.password===this.state.confirmPassword) {
            this.setState({ loading: true });
            firebase.auth()
                .createUserWithEmailAndPassword(this.state.email, this.state.password)
                .then(result => this.createNewUserOnSystem(result.user.uid, this.state.name, this.state.email, this.state.tags))
                .catch(err => this.showDialogWithMessage(JSON.stringify(err)));
        }
        else this.showDialogWithMessage('Passwords do not match.');
    }

    createNewUserOnSystem(uid, name, email, tags) {
        tags = tags.split(',');
        const categoriesArray = [];
        this.state.life? categoriesArray.push('life') : null;
        this.state.relationship? categoriesArray.push('relationship') : null;
        this.state.executive? categoriesArray.push('executive') : null;
        this.state.career? categoriesArray.push('career') : null;
        this.state.performance? categoriesArray.push('performance') : null;
        const coachObj = { _id: uid, name: name, email: email, tags: tags, primary_categories: categoriesArray };


        const userId = uid;
                axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
                    .then(resp => {
                        const token = resp.data.token;
                        axios.post(config.HOSTNAME+'/counsellors', coachObj, { headers: ({ 'Authorization': 'Bearer ' + token }) })
                            .then(resp => { this.props.navigation.navigate('HomeCoach', { coach: resp.data, token: token }) })
                            .catch(err => Alert.alert(APPNAME, err));
                    })
                    .catch(err => this.showDialogWithMessage(JSON.stringify(err)))
                    .finally(() => this.setState({ loading: false }));

        // axios.post(config.HOSTNAME+'/counsellors', coachObj)
        //     .then(resp => this.props.navigation.navigate('HomeCoach', { coach: resp.data }))
        //     .catch(err => this.showDialogWithMessage(JSON.stringify(err)));
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        flex: 1,
        paddingLeft: 16,
        paddingRight: 16,
        paddingTop: 32
    },
    imageStyle: {
        height: 49,
        width: 190,
        marginBottom: 32,
        alignSelf: 'center'
    },
    titleTextStyle: {
        color: config.COLOR_PRIMARY,
        fontSize: 32,
        fontFamily: 'OpenSans-Bold'
    },
    subtitleTextStyle: {
        color: config.COLOR_TEXT_DARK,
        fontFamily: 'OpenSans-Regular',
        fontSize: 32
    },
    textInputStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        marginTop: 10
    },
    signUpTextStyle: {
        marginTop: 8,
        fontSize: 16,
        color: config.COLOR_TEXT_DARK,
        fontFamily: 'OpenSans-Regular',
        alignSelf: 'center'
    },
    counsellorLoginTextStyle: {
        marginTop: 32,
        fontSize: 16,
        color: config.COLOR_TEXT_LIGHT,
        fontFamily: 'OpenSans-Regular',
        alignSelf: 'center'
    },
    categoriesStyle: {
        justifyContent: 'space-evenly'
    },
    categoryItemViewStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 8
    },
    categoryItemTextStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular'
    }
});

export default SignUpCoach;