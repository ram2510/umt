
import React from 'react';

import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    TextInput,
    Image,
    Alert,
    TouchableWithoutFeedback,
    FlatList,
    Button,
    ActivityIndicator
} from 'react-native';
import axios from 'axios';
// import firebase from 'firebase/app';
// import 'firebase/storage';
// import ImagePicker from 'react-native-image-picker';
// import RNFetchBlob from 'rn-fetch-blob';
//LOCAL
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import config from '../../../config';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class SearchPage extends React.Component {
   
    
    state = {
        query: '',
        coaches: [],
        loading:true,
        isSearching:false
    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })
    }


    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle
        } = styles;
      //  const { search } = this.state;

        const { navigation } = this.props;
        const user = this.user
        const token = this.token

        return (
            <ScrollView style={containerStyle}>
            <View style={contentStyle}>
                <TitleBar
                    leftIconName='ios-arrow-round-back'
                    leftIconFunction={ () => navigation.goBack() }
                    rightIconName ='ios-search'
                    rightIconFunction = { () => this.onPressSearch()}
                />
                <TextInput
                        placeholder='Search Coaches'
                        selectionColor={config.COLOR_PRIMARY}
                        underlineColorAndroid={config.COLOR_PRIMARY}
                        placeholderTextColor={config.COLOR_TEXT_LIGHT}
                        value={this.state.query} // have to pass default value here
                        onChangeText={ text => this.setState({ query: text }) }
                        onSubmitEditing={() => this.onPressSearch()}
                    />
                    {(this.state.isSearching?
                    <View style={styles.empty}>
                        <ActivityIndicator color="#D3D3D3" size='large' />
                        <Text style={styles.mssg}>
                            Searching ..
                        </Text>
                    </View>
                    :<FlatList
                    data={ this.state.coaches} //have to pass default value here
                    renderItem={ ({item}) => this.renderCoachListItem(item, user) }
                    keyExtractor={ (item, index) => index }
                />)}
                
            </View>

            </ScrollView>
        );
    }

    renderCoachListItem(coach, user) {
        const {
            coachImageStyle,
            
            starImageStyle
        } = styles;


        var URI= "https://i.ibb.co/Rzpf2cB/zero.png" ;

       

        if (coach.rating == 5){
            URI = "https://i.ibb.co/6PzSjWf/five.png" 
        } else if (coach.rating == 4){
            URI= "https://i.ibb.co/8bWGFRQ/four.png" 
        } else if (coach.rating == 3){
            URI= "https://i.ibb.co/pbL052Q/three.png" 
        } else if (coach.rating == 2){
            URI= "https://i.ibb.co/sP89GwK/two.png" 
        } else if (coach.rating == 1){
            URI= "https://i.ibb.co/PgC6hZw/one.png" 
        } else if (coach.rating == 0){
           URI= "https://i.ibb.co/Rzpf2cB/zero.png" 
       } else if (coach.rating == null){
           URI= "https://i.ibb.co/Rzpf2cB/zero.png" 
       }

        return (
            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user })}>
            <View style={[{
                width: "100%", borderBottomColor: config.COLOR_PRIMARY, borderBottomWidth: 1, paddingBottom: 20, paddingTop: 20
            }]}>


                <View style={[{ flexDirection: 'row', width: 300 }]}>
                    <Image
                        style={coachImageStyle}
                        source={{ uri: coach.image || 'https://fakeimg.pl/128x128/' }}
                    />
                    <View style={[{ width: 250, paddingLeft: 16, paddingRight: 16 }]}>
                        <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold' , marginBottom: 3}}>{coach.name}</Text>
                        <Image source={{ uri: URI }
                        } style={starImageStyle} />

                    </View>
                </View>

                <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.bio}</Text>
                <View style={[{ width: "100%", paddingBottom: 8, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row', marginTop:20 }]}>

                <OutlineButton
                            text='Book Now'
                            // color={config.COLOR_PRIMARY}
                            onPress={() => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user })}
                        />
                </View>
            </View>

        </TouchableWithoutFeedback>
            // <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user })}>
            //     <View style={[{
            //         width: "100%", borderBottomColor: config.COLOR_PRIMARY, borderBottomWidth: 1, paddingBottom: 20, paddingTop: 20
            //     }]}>


            //         <View style={[{ flexDirection: 'row', width: 300 }]}>
            //             <Image
            //                 style={coachImageStyle}
            //                 source={{ uri: coach.image || 'https://fakeimg.pl/128x128/' }}
            //             />
            //             <View style={[{ width: 250, paddingLeft: 16, paddingRight: 16 }]}>
            //                 <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold' }}>{coach.name}</Text>
            //                 <Image source={{ uri: URI }
            //                 } style={starImageStyle} />
            //                 <View style={[{ width: 100, paddingBottom: 8 }]}>

            //                     <Button
            //                         title='Book Now'
            //                         color={config.COLOR_PRIMARY}
            //                         onPress={() => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user })}
            //                     />
            //                 </View>
            //             </View>
            //         </View>

            //         <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.bio}</Text>

            //     </View>

            // </TouchableWithoutFeedback>

            // <TouchableWithoutFeedback onPress={ () => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user }) }>
            //     <View style={coachItemStyle}>
            //         <Image
            //             style={coachImageStyle}
            //             source={{uri: coach.image||'https://fakeimg.pl/128x128/'}}
            //         />
            //         <View style={coachContentStyle}>
            //             <Text style={{color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold'}}>{coach.name}</Text>
            //             <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular'}}>{coach.bio}</Text>

            //             <Image source={ {uri: URI }
            //             } style={starImageStyle} />
                        

            //             {/* <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', marginBottom: 16}}>{coach.rating}</Text> */}
            //             <View style={[{ width: 100 }]}>

            //             <Button
            //                 title='Book Now'
            //                 color={config.COLOR_PRIMARY}
            //                 onPress={ () => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user }) }
            //             />
            //             </View>
            //         </View>
            //     </View>
            // </TouchableWithoutFeedback>
        );
    }


    async onPressSearch() {
        if(!this.state.isSearching){
        this.setState({
            isSearching:true
        })
        const { navigation } = this.props;
        if (this.state.query != null){
            let token = await AsyncStorage.getItem("token")
            token = JSON.parse(token)
            var query = this.state.query;
            axios.get('https://guarded-gorge-25448.herokuapp.com/api/v1/counsellors/search?query='+query, { headers: ({'Authorization': 'Bearer '+ token}) })
            .then(resp => {
                const coaches = resp.data;
                if(coaches.length==0)
                this.setState({ coaches: coaches,isSearching:false });
            })
            .catch(err => {
                Alert.alert(config.APP_NAME, config.ERROR_MESSAGE)
                this.setState({isSearching:false})
            });

        };
    }

    }


}




const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    // coachItemStyle: {
    //     borderBottomColor: 'gray',
    //     borderBottomWidth: 1,
    //     flexDirection: 'row',
    //     flex: 1,
    //     alignItems: 'center',
    //     height: 128,
    // },
    coachImageStyle: {
        height: 96,
        width: 96,
        borderRadius: 48
    },
    // coachContentStyle: {
    //     justifyContent: 'space-evenly',
    //     paddingLeft: 16
    // },
    starImageStyle: {
        marginTop: 10,
        height: 28,
        width: 100,
        resizeMode: 'cover',
    },
    empty:{
        flex: 1,
        height: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        width: "100%",
        marginTop: "50%",
        padding: 10
    },
    mssg: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
      }
});

export default SearchPage;

