import React from 'react';
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,
    Button,
    FlatList,
    TouchableWithoutFeedback,
    Alert
} from 'react-native';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'


class CoachesListPage extends React.Component {
    state = {
        coaches: [],
        loading:true,
    }
    constructor(props){
        super(props)
        this.user;
        this.token;
        this.getData()
    }

    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        // console.log(this.user)
        this.user = JSON.parse(this.user)
        // console.log('l')
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        // this.setState({
        //     loading:false
        // })
    }



    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle
        } = styles;
        const { navigation } = this.props;
        const coachingType = navigation.getParam('type', null);
        const coachingTypeName = coachingType.key;
        const user = this.user
        const token = this.token

        return (
            <ScrollView style={containerStyle}>
                <View style={contentStyle}>
                    <TitleBar
                        leftIconName='ios-arrow-round-back'
                        leftIconFunction={() => navigation.goBack()}
                        titleText={coachingTypeName}
                    />
                    <FlatList
                        data={this.state.coaches}
                        renderItem={({ item }) => this.renderCoachListItem(item, user, token)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </ScrollView>
        );
    }

    renderCoachListItem(coach, user, token) {
        const {
            coachImageStyle,
            coachItemStyle,
            coachContentStyle,
            starImageStyle,

        } = styles;




        var URI = "https://i.ibb.co/Rzpf2cB/zero.png";



        if (coach.rating == 5) {
            URI = "https://i.ibb.co/6PzSjWf/five.png"
        } else if (coach.rating == 4) {
            URI = "https://i.ibb.co/8bWGFRQ/four.png"
        } else if (coach.rating == 3) {
            URI = "https://i.ibb.co/pbL052Q/three.png"
        } else if (coach.rating == 2) {
            URI = "https://i.ibb.co/sP89GwK/two.png"
        } else if (coach.rating == 1) {
            URI = "https://i.ibb.co/PgC6hZw/one.png"
        } else if (coach.rating == 0) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        } else if (coach.rating == null) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        }





        return (

            <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user, token: token})}>
                <View style={[{
                    width: "100%", borderBottomColor: config.COLOR_PRIMARY, borderBottomWidth: 1, paddingBottom: 20, paddingTop: 20
                }]}>


                    <View style={[{ flexDirection: 'row', width: 300 }]}>
                        <Image
                            style={coachImageStyle}
                            source={{ uri: coach.image  || 'https://fakeimg.pl/128x128/'
                                 }}
                        />
                        <View style={[{ width: 250, paddingLeft: 16, paddingRight: 16 }]}>
                            <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold' , marginBottom: 3}}>{coach.name}</Text>
                            <Image source={{ uri: URI }
                            } style={starImageStyle} />

                        </View>
                    </View>

                    <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.bio}</Text>
                    <View style={[{ width: "100%", paddingBottom: 8, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row', marginTop:20 }]}>

                        <OutlineButton
                            text='Book Now'
                            // color={config.COLOR_PRIMARY}
                            onPress={() => this.props.navigation.navigate('CoachBookPage', { coach: coach, user: user, token: token })}
                        />
                    </View>
                </View>

            </TouchableWithoutFeedback>

            //             /* // <TouchableWithoutFeedback onPress={() => this.props.navigation.navigate('CoachProfilePage', { coach: coach, user: user })}>
            //             //     <View style={coachItemStyle}>
            //             //         
            //             //         <View style={coachContentStyle}>

            //           

            //             //             {/* <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', marginBottom: 16 }}>{coach.rating}</Text> */}
            // {/* 
            //             //             <View style={[{ width: 100 }]}>
            //             //                

            //             //             </View> */}


            //             {/* //         </View> */}

            //             {/* //     </View> */} 

        )

    }

    async componentDidMount() {
        const { navigation } = this.props;
        const coachingType = navigation.getParam('type', null);
        // console.log(coachingType)
        let token = await AsyncStorage.getItem("token")
        token = JSON.parse(token)
        // console.log(token)
        const coachingTypeName = coachingType.value;



        axios.get(config.HOSTNAME + '/counsellors/categories/' + coachingTypeName, { headers: ({ 'Authorization': 'Bearer ' + token }) })
            .then(resp => {
                // console.log(resp)
                const coaches = resp.data;
                this.setState({ coaches: coaches,loading:false });
            })
            .catch(err =>{ 
                Alert.alert(config.APP_NAME,config.ERROR_MESSAGE)
                this.setState({
                    loading:false
                })
                //console.log(err)
            })
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    // coachItemStyle: {
    //     borderBottomColor: 'gray',
    //     borderBottomWidth: 1,
    //     flexDirection: 'row',
    //     //flex: 1,
    //     alignItems: 'center',
    //     height: 128,
    // },
    coachImageStyle: {
        height: 96,
        width: 96,
        borderRadius: 48
    },
    // coachContentStyle: {
    //     justifyContent: 'space-evenly',
    //     paddingLeft: 16
    // },
    starImageStyle: {
        marginTop: 10,
        height: 28,
        width: 100,
        resizeMode: 'cover',
    },

});

export default CoachesListPage;
