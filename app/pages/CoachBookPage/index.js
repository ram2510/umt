import React from 'react';
import {
    View,
    StyleSheet,
    Text,
    ScrollView,
    Image,
    Button,
    TextInput,
    Alert
} from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import axios from 'axios';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

const months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec' ];
const currentDate = new Date();

class CoachBookPage extends React.Component {
    state = {
        selectedDate: null,
        selectedTime: null,
        goal: '',
        loading:true,
    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        console.log(this.token)
        this.setState({
            loading:false
        })

    }

    render() {

        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle,
            contentStyle,
            bookingDateAndTimeTextStyle,
            textInputStyle
        } = styles;

        const { navigation } = this.props;
        const coach = navigation.getParam('coach', null);
        const user =  this.user
        const coachTimings = coach.timings;

        const currentDay = currentDate.getDate();
        const month = months[currentDate.getMonth()];

        const dateProps = [
            {label: month+' '+(currentDay+1), value: 0 },
            {label: month+' '+(currentDay+2), value: 1 },
            {label: month+' '+(currentDay+3), value: 2 },
            {label: month+' '+(currentDay+4), value: 3 },
        ];

        const timeProps = [
            { label: '13:00 Hrs', value: 13 },
            { label: '14:00 Hrs', value: 14 },
            { label: '15:00 Hrs', value: 15 },
            { label: '16:00 Hrs', value: 16 },
        ];

        return (
            <ScrollView style={containerStyle}>
            <View style={contentStyle}>
                <TitleBar
                        leftIconName='ios-arrow-round-back'
                        leftIconFunction={ () => navigation.goBack() }
                        titleText={ config.APP_NAME }
                    />

                <View style={coachItemStyle}>
                    <Image
                        style={coachImageStyle}
                        source={{uri: coach.image||'https://fakeimg.pl/128x128/'}}
                    />
                    <View style={coachContentStyle}>
                        <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{coach.name}</Text>
                        <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular', marginBottom: 8}}>{coach.bio}</Text>
                        <Button
                            title='INR 800/hr'
                            color={config.COLOR_PRIMARY}
                        />
                    </View>
                </View>

                <View style={{margin: 16}} />

                <RadioForm
                    radio_props={dateProps}
                    initial={0}
                    buttonColor={config.COLOR_PRIMARY}
                    formHorizontal
                    labelHorizontal
                    animation
                    buttonWrapStyle={{marginLeft: 8}}
                    onPress={ date => this.setState({ selectedDate: date }) }
                />
                
                <View style={{margin: 16}} />

                <RadioForm
                    radio_props={timeProps}
                    initial={0}
                    buttonColor={config.COLOR_PRIMARY}
                    labelHorizontal
                    animation
                    buttonWrapStyle={{marginLeft: 8}}
                    onPress={ time => this.setState({ selectedTime: time }) }
                />

                <TextInput
                    placeholder='Mention your expectations from this coach'
                    multiline
                    placeholderTextColor={config.COLOR_TEXT_DARK}
                    selectionColor={config.COLOR_PRIMARY}
                    underlineColorAndroid={config.COLOR_PRIMARY}
                    style={textInputStyle}  
                    value={ this.state.goal }
                    onChangeText={ text => this.setState({ goal: text }) }
                />

                <Button
                    title='Book coach'
                    color={config.COLOR_PRIMARY}
                    onPress={ this.onBookButtonPress.bind(this) }
                />
            </View>
            </ScrollView>
        );
    }

    onBookButtonPress() {
        
        const { navigation } = this.props;
        const coach = navigation.getParam('coach', null);
        const user = this.user
        const token = this.token
        console.log(token)
        if(user.phone){
            const month = currentDate.getMonth();
            const year = currentDate.getFullYear();
            const dateBooked = currentDate.getDate() + (this.state.selectedDate+1);
            const timeBooked = this.state.selectedTime;
            const bookedDate = new Date(year, month, dateBooked, timeBooked, 0, 0, 0);
            const goal = this.state.goal;
            const currentUserId = user._id;
            const coachId = coach._id || '12345abcde';  //CHANGE
            const rate = coach.rate || 800; //CHANGE
            const userPhone = user.phone
            const coachPhone = coach.phone
            const booking = { 
                user_id: currentUserId, 
                counsellor_id: coachId, 
                date_time: bookedDate, 
                goal: goal, 
                rate: rate,
                user_phone:userPhone,
                counsellor_phone:coachPhone 
            };
            axios.post(config.HOSTNAME+'/booking', booking,{ headers: { 'Authorization': 'Bearer ' + token } })
            .then(response => {
                const respData = response.data;
                this.showDialogMessage(JSON.stringify(respData));
            }).catch(err =>{ 
                this.showDialogMessage(JSON.stringify(err))
                console.log(err.response)
            });    
        }else{
            Alert.alert(config.APP_NAME, "Please enter your mobile number before trying to book a coach")
        }
    }

    showDialogMessage(message) {
        Alert.alert(config.APP_NAME, message);
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        height: 128,
        marginBottom: 16
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-between'
    },
    bookingDateAndTimeTextStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Bold',
        marginBottom: 8,
        marginTop: 8
    },
    textInputStyle: {
        color: config.COLOR_TEXT_DARK,
        fontSize: 16,
        fontFamily: 'OpenSans-Regular',
        marginBottom: 32,
        marginTop: 16
    }
});

export default CoachBookPage;