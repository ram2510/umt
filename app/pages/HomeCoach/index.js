import React from 'react';
import {
    ScrollView,
    View,
    StyleSheet,
    Text,
    Alert,
    FlatList,
    Image,
    Button,
    TouchableWithoutFeedback
} from 'react-native';
//LOCAL
import config from '../../../config';
import TitleBar from '../../components/TitleBar';
import OutlineButton from '../../components/OutlineButton';
import AsyncStorage from '@react-native-community/async-storage';
import SpinnerScreen from '../../components/SpinnerScreen'

class HomePatient extends React.Component {
    state = {
        user: null,
        loading:true,

    }
    constructor(props){
        super(props)
        this.user = {}
        this.token = {}
        this.getData()
    }
    getData=async ()=>{
        this.user =  await AsyncStorage.getItem('userData')
        this.user = JSON.parse(this.user)
        this.token = await AsyncStorage.getItem("token")
        this.token = JSON.parse(this.token)
        this.setState({
            loading:false
        })

    }
    render() {
        if(this.state.loading){
            return(
                <SpinnerScreen message="Loading .." />
            )
        }
        const {
            containerStyle,
            contentStyle,
            coachContentStyle,
            coachImageStyle,
            coachItemStyle,
            titleTextStyle,
            appointmentsContainerViewStyle,
            starImageStyle
        } = styles;

        const { navigation } = this.props;
        const coach = this.user
        const token = this.token

        var URI = "https://i.ibb.co/Rzpf2cB/zero.png";



        if (coach.rating == 5) {
            URI = "https://i.ibb.co/6PzSjWf/five.png"
        } else if (coach.rating == 4) {
            URI = "https://i.ibb.co/8bWGFRQ/four.png"
        } else if (coach.rating == 3) {
            URI = "https://i.ibb.co/pbL052Q/three.png"
        } else if (coach.rating == 2) {
            URI = "https://i.ibb.co/sP89GwK/two.png"
        } else if (coach.rating == 1) {
            URI = "https://i.ibb.co/PgC6hZw/one.png"
        } else if (coach.rating == 0) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        } else if (coach.rating == null) {
            URI = "https://i.ibb.co/Rzpf2cB/zero.png"
        }


        return (
            <ScrollView style={containerStyle}>
                <View>
                    <TitleBar
                        leftIconName='ios-search'
                        rightIconName='ios-settings'
                        titleText={config.APP_NAME}
                    />


                    <View style={[{
                        width: "100%", borderBottomColor: config.COLOR_PRIMARY, borderBottomWidth: 1, padding: 20
                    }]}>


                        <View style={[{ flexDirection: 'row', width: "70%" }]}>
                            <Image
                                style={coachImageStyle}
                                source={{
                                    uri: coach.image || 'https://fakeimg.pl/128x128/'
                                }}
                            />
                            <View style={[{ width: "100%", paddingLeft: 16, paddingRight: 16 }]}>
                                <Text style={{ color: 'black', fontSize: 18, fontFamily: 'OpenSans-Bold', marginBottom: 3 }}>{coach.name}</Text>
                                <Image source={{ uri: URI }
                                } style={starImageStyle} />
                                <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.email}</Text>
                                <Text style={{ color: config.COLOR_TEXT_DARK, fontSize: 14, fontFamily: 'OpenSans-Regular', paddingTop: 10 }}>{coach.bio}</Text>

                                <View style={[{ width: "70%", paddingBottom: 8, alignItems: 'flex-end', justifyContent: 'flex-end', flexDirection: 'row', marginTop: 20 }]}>

                                    <OutlineButton
                                        text='Edit Profile'
                                        // color={config.COLOR_PRIMARY}
                                        onPress={() => this.props.navigation.navigate('EditProfileCoach', { coach: coach, token: token })}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                    {/* <View style={contentStyle}>
                    <View style={coachItemStyle}>
                        <Image
                            style={coachImageStyle}
                            source={{uri: coach.image||'https://fakeimg.pl/128x128/'}}
                        />
                        <View style={coachContentStyle}>
                            <Text style={{color: 'black', fontSize: 20, fontFamily: 'OpenSans-Bold'}}>{coach.name}</Text>
                            <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular'}}>{coach.email}</Text>
                            <Text style={{color: config.COLOR_TEXT_DARK, fontSize: 16, fontFamily: 'OpenSans-Regular', marginTop: 8}}>{coach.bio}</Text>
                            <Button
                                title='Edit Profile'
                                onPress={ () => navigation.navigate('EditProfileCoach', { coach: coach , token: token}) }
                                color={config.COLOR_PRIMARY}
                            />
                        </View>
                    </View> */}

                    <View style={appointmentsContainerViewStyle}>
                        <Text style={titleTextStyle}>{'Upcoming'}</Text>
                    </View>

                    <View style={appointmentsContainerViewStyle}>
                        <Text style={titleTextStyle}>{'Previous'}</Text>
                    </View>

                    <View style={appointmentsContainerViewStyle}>
                        <Text style={titleTextStyle}>{'Feedback'}</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }

    showMessageDialog(message) {
        Alert.alert(config.APP_NAME, message);
    }
};

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        backgroundColor: 'white'
    },
    contentStyle: {
        paddingLeft: 16,
        paddingRight: 16
    },
    coachItemStyle: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        height: 128
    },
    coachImageStyle: {
        height: 128,
        width: 128,
        borderRadius: 64
    },
    coachContentStyle: {
        justifyContent: 'space-evenly',
        marginLeft: 16
    },
    titleTextStyle: {
        color: config.COLOR_PRIMARY,
        fontSize: 24,
        fontFamily: 'OpenSans-Bold'
    },
    appointmentsContainerViewStyle: {
        marginTop: 32
    },
    starImageStyle: {
        height: 28,
        width: 100,
        resizeMode: 'cover',
    }
});

export default HomePatient;