import React from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    Text
} from 'react-native';
import * as Animatable from 'react-native-animatable';
// import firebase from 'firebase/app';
// import 'firebase/auth';
//LOCAL
import config from '../../../config';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios'
class Splash extends React.Component {

    constructor(props){
        super(props)
        setTimeout( this.authUser,2000)
    }
    
    authUser=async ()=>{
			// console.log("l");
        try {
						// await AsyncStorage.removeItem('userData')
            let userData =  await AsyncStorage.getItem('userData')
            // console.log(userData);
				if(userData){
                    userData = JSON.parse(userData)
                    userId = userData._id;  
                    let tokenResp = await axios.post('https://guarded-gorge-25448.herokuapp.com/api/v1/authenticate/' + userId)
                    const token = tokenResp.data.token;
                    await AsyncStorage.removeItem('token')
                    await AsyncStorage.setItem('token',JSON.stringify(token))
                    let userType = userData.userType
                    this.props.navigation.navigate(userData ?  `Home${userType}` : 'SignIn'); 
                    return       
                }else{
                    this.props.navigation.navigate('SignIn')
                }
    } catch (e) {
		// saving error
		console.log(e)
    this.props.navigation.navigate('SignIn')
    }
    }

    render() {
        const {
            containerStyle,
            textStyle
        } = styles;

        return (
            <View style={containerStyle}>
                <StatusBar hidden />

                <Animatable.Text animation='bounceInRight' duration={1500}>
                    <Text style={textStyle}>{ config.APP_NAME }</Text>
                </Animatable.Text>
            </View>
        );
    }

    
};

const styles = StyleSheet.create({
    containerStyle: {
        backgroundColor: config.COLOR_PRIMARY,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 16
    },
    textStyle: {
        color: 'white',
        fontFamily: 'OpenSans-Bold',
        fontSize: 64
    }
});

export default Splash;