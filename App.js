// import React from 'react';
import { createAppContainer, createStackNavigator,createSwitchNavigator } from 'react-navigation';
//LOCAL
import Splash from './app/pages/Splash';
import SignIn from './app/pages/SignIn';
import SignUpPatient from './app/pages/SignUpPatient';
import HomePatient from './app/pages/HomePatient';
import CoachesListPage from './app/pages/CoachesListPage';
import CoachProfilePage from './app/pages/CoachProfilePage';
import CoachBookPage from './app/pages/CoachBookPage';
import Settings from './app/pages/Settings';
import EditProfile from './app/pages/EditProfile';
import SignInCoach from './app/pages/SignInCoach';
import SignUpCoach from './app/pages/SignUpCoach';
import HomeCoach from './app/pages/HomeCoach';
import EditProfileCoach from './app/pages/EditProfileCoach';
import SearchPage from './app/pages/SearchPage';
import UserBookings from './app/pages/UserBookings';
import UpcomingBookings from './app/pages/UpcomingBookings';

console.disableYellowBox = true;
const PatientLoginStack=createSwitchNavigator({
  Splash: { screen: Splash },
  SignIn: { screen: SignIn },
  SignUpPatient: { screen: SignUpPatient },
  HomePatient: { screen: HomePatient }, 
},{
  initialRouteName: 'Splash'
})

const CoachLoginStack = createSwitchNavigator({
  Splash: { screen: Splash },
  SignInCoach: { screen: SignIn },
  SignUpCoach: { screen: SignUpCoach },
  HomeCoach: { screen: HomeCoach },
},{
  initialRouteName: 'Splash'
})
const AppNavigator = createStackNavigator(
  {
    PatientLoginStack:{screen:PatientLoginStack},
    CoachLoginStack:{screen:CoachLoginStack},
    CoachesListPage: { screen: CoachesListPage },
    CoachProfilePage: { screen: CoachProfilePage },
    CoachBookPage: { screen: CoachBookPage },
    Settings: { screen: Settings },
    EditProfile: { screen: EditProfile },
    EditProfileCoach: { screen: EditProfileCoach },
    SearchPage: { screen : SearchPage },
    UserBookings: { screen: UserBookings},
    UpcomingBookings: { screen: UpcomingBookings},
    SignInCoach:{screen:SignInCoach}
  },
  {
    headerMode: 'none',
    // initialRouteName: 'Splash'
  }
);

export default createAppContainer(AppNavigator);
