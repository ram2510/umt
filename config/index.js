module.exports = {
    APP_NAME: 'Unlock Me Today',
    COLOR_PRIMARY: '#F88A42',
    COLOR_TEXT_DARK: '#424242',
    COLOR_TEXT_LIGHT: '#BDBDBD',
    COLOR_TEXT_WHITE:"#ffffff",
    COLOR_TEXT_BLACK:"#000",
    WEB_CLIENT_ID: '936662275132-0jn4fcgu31kocp676shkciulga7eukqg.apps.googleusercontent.com',
    HOSTNAME: 'https://guarded-gorge-25448.herokuapp.com/api/v1',
    ERROR_MESSAGE:"An unexpected error occured please try again later"
};